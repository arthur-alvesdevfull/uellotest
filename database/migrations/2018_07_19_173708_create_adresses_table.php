<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdressesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adresses', function(Blueprint $table)
		{
			$table->increments('id_address');
			$table->integer('id_client')->unsigned()->nullable()->index('adresses_id_client_foreign');
			$table->string('logradouro', 191);
			$table->string('numero', 191);
			$table->string('complemento', 191)->nullable();
			$table->string('bairro', 191);
			$table->string('cidade', 191);
			$table->string('estado', 191);
			$table->string('cep', 191);
			$table->string('longitude', 191);
			$table->string('latitude', 191);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adresses');
	}

}
