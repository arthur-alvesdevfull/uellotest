<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
Route::get('/','HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/newclient', 'ClientController@index')->name('register.client');
Route::post('/registerclient', 'ClientController@create')->name('create.client');
Route::get('/extraircsv', 'ClientController@extrair')->name('extrair.client');
Route::get('/importarcsv', 'ClientController@importar')->name('importar.client');
Route::get('/uploadcsv', 'ClientController@uploadcsv')->name('importar.client');
