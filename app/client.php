<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Address;
use Carbon\Carbon;
use League\Csv\Writer;
use Illuminate\Support\Facades\DB;
class Client extends Model
{
    protected $primaryKey = 'id_client';
    protected $fillable = ['nome','email','data_nascimento','cpf'];
    
    public function address()
    {
        return $this->hasOne(Address::class,'id_client','id_client');
    }
    public function extrair()
    {
        $datas = DB::select('SELECT
        cli.nome,
        cli.email,
        cli.cpf,
        cli.data_nascimento,
        ad.logradouro,
        ad.numero,
        ad.complemento,
        ad.bairro,
        ad.cidade,
        ad.estado
    FROM
        clients cli
    INNER JOIN adresses ad USING(id_client)');
        if(!empty($datas)){
            $csv = Writer::createFromFileObject(new \SplTempFileObject());

            $csv->insertOne(["Nome","Email","CPF","Data_Nascimento","Logradouro","Numero","Complemento","Bairro","Cidade","Estado"]);

            $datas = collect($datas)->map(function($x){ return (array) $x; })->toArray();

            foreach($datas as $data){
                $csv->insertOne($data);
            }
            $csv->output('clients_'.Carbon::now().'.csv');
            print 1;
        }
        
        
    }
    
}
