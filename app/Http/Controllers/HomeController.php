<?php

namespace App\Http\Controllers;

use DateTime;
use  App\Client;
use Illuminate\Http\Request;
use  \Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = DB::table('clients')->join('adresses', 'clients.id_client', '=', 'adresses.id_client')->get();
        foreach($clients as $client){
            $date = new DateTime($client->data_nascimento);
            $client->data_nascimento = $date->format('d/m/Y');
            // $client->cpf = $this->mask($client->cpf, '###.###.###-##');
            // $client->cep = $this->mask($client->cep, '#####-###');
        }
       return view('home',['clients'=>$clients]);
    }
    /*function mask( $value , $mask ){
        $value = preg_replace( '/[^\d]+/' , '' , $value );
    
        if ( $mask === '#' ) return $value;
        else {
            for ( $i = 0 , $j = 0 , $t = strlen( $mask ) , $fi = $fo = null ; $i < $t ; $i++ ){
                if ( ( $mask{ $i } !== '#' ) && $j ){
                    $fi  = sprintf( '%s%%%dd' , $fi , $j );
                    $fo = sprintf( '%s%%0%dd%s' , $fo , $j , $mask{ $i } );
                    $j = 0;
                } elseif ( $mask{ $i } == '#' ) ++$j;
                else $fo .= $mask{ $i };
            }
    
            if ( $j ){
                $fi = sprintf( '%s%%%dd' , $fi , $j );
                $fo = sprintf( '%s%%0%dd' , $fo , $j );
            }
    
            return vsprintf( $fo , sscanf( $value , $fi ) );
        }
    }*/
        
}
