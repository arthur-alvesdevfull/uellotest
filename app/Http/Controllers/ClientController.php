<?php

namespace App\Http\Controllers;
use \Excel;
use DateTime;
use App\Client;
use App\Address;
use Carbon\Carbon;
use League\Csv\Reader;
use League\Csv\Writer;
use Illuminate\Http\Request;
use League\Flysystem\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Collections\CellCollection;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
       
        $data = $request->all();
        $data['cpf'] = str_replace(['.','-'],['',''],$data['cpf']);
        $data['cep'] = str_replace(['.','-'],['',''],$data['cep']);
        if(empty($data['complemento'])){
            $data['complemento'] = 'Não Há Complemento';
        }
        $client = new Client();
        $id = $client->create($data)->id_client;

        $data['id_client'] = $id;

        $address = new Address();
        $address->create($data);

        print true;
    }


    public function extrair()
    {
            $datas = DB::select('SELECT
            cli.nome,
            cli.email,
            DATE_FORMAT(cli.data_nascimento, "%d-%m-%Y") AS data_nascimento,
            cli.cpf,
            CONCAT(ad.logradouro," , ",ad.numero,IFNULL(CONCAT(" - ",ad.complemento),"")," - ",ad.bairro," - ", ad.cidade," - ", ad.estado) AS endereco,
            ad.cep
        FROM
            clients cli
        INNER JOIN adresses ad USING(id_client)');
    $arr1 = [];
    foreach($datas as $arr ){
        foreach($arr as $a){
            $arr->nome  = $this->removeAccents($arr->nome);
            $arr->email = $this->removeAccents($arr->email);
            $arr->endereco = $this->removeAccents($arr->endereco);
        }
        $arr1[] =$arr;
    }
        
        if(!empty($datas)){
            
            $csv = Writer::createFromFileObject(new \SplTempFileObject());

            $csv->insertOne(["Nome","Email","Data_Nascimento","CPF","Endereco","Cep"]);

            $datas = collect($datas)->map(function($x){ return (array) $x; })->toArray();
           // dd($datas);
             foreach($datas as $data){
                $csv->insertOne($data);
            }
            $csv->output('clients_'.Carbon::now().'.csv');
        }
        
        
    }
    
    public function uploadcsv(Request $request){
        $data = $request->all();
        
        $data = $data['user'];
      
        
        if(!empty($data)){
            $client = new Client();
            
            $cpf = str_replace(['.','-'],['',''],$data['cpf']);
            $nas = date_create($data['datanasc']);
            $nas = date_format($nas, 'Y-m-d');

            $id = $client->create([
                                    "nome"=>strtoupper($data['nome']),
                                    "email"=>$data['email'],
                                    "cpf"=>$cpf,
                                    "data_nascimento"=>$nas
                                    ])->id_client;

            $address = new Address();

            $complemento = isset($data['subpremise'])?strtoupper($data['subpremise']):null;
            $address->create([
                                'id_client'=>$id,
                                'logradouro'=>strtoupper($data['route']),
                                'numero'=>$data['street_number'],
                                'complemento'=>$complemento,
                                'bairro'=>strtoupper($data['political']),
                                'cidade'=>strtoupper($data['administrative_area_level_2']),
                                'estado'=>strtoupper($data['administrative_area_level_1']),
                                'cep'=>$data['postal_code'],
                                'longitude'=>$data['lng'],
                                'latitude'=>$data['lat']]
                            );

            print true;
        }
    }
    public function removeAccents( $string ){
        return preg_replace(
            array(
                //Maiúsculos
                '/\xc3[\x80-\x85]/',
                '/\xc3\x87/',
                '/\xc3[\x88-\x8b]/',
                '/\xc3[\x8c-\x8f]/',
                '/\xc3([\x92-\x96]|\x98)/',
                '/\xc3[\x99-\x9c]/',
        
                //Minúsculos
                '/\xc3[\xa0-\xa5]/',
                '/\xc3\xa7/',
                '/\xc3[\xa8-\xab]/',
                '/\xc3[\xac-\xaf]/',
                '/\xc3([\xb2-\xb6]|\xb8)/',
                '/\xc3[\xb9-\xbc]/',
            ),
            str_split( 'ACEIOUaceiou' , 1 ),
            $this->is_utf8( $string ) ? $string : utf8_encode( $string )
        );
        }
       public function is_utf8( $string ){
            return preg_match( '%^(?:
                 [\x09\x0A\x0D\x20-\x7E]
                | [\xC2-\xDF][\x80-\xBF]
                | \xE0[\xA0-\xBF][\x80-\xBF]
                | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}
                | \xED[\x80-\x9F][\x80-\xBF]
                | \xF0[\x90-\xBF][\x80-\xBF]{2}
                | [\xF1-\xF3][\x80-\xBF]{3}
                | \xF4[\x80-\x8F][\x80-\xBF]{2}
                )*$%xs',
                $string
            );
            }
   
}
