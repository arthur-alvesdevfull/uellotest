<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Client;

class Address extends Model
{
    protected $table = 'adresses';
    protected $primaryKey = 'id_address';

    protected $fillable = ['id_client','logradouro','numero','complemento','bairro','cidade','estado','cep','longitude','latitude'];
    
    public function client()
    {
        return $this->belongsTo(Client::class,'id_client','id_client');
    }
}
