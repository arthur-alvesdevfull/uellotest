@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="py-4 text-center">Cadastro de Clientes</h1>
            <form method="post" id="frm_client">
                {{ csrf_field() }}
               <button type="submit" class="btn btn-outline-danger btn-cli" id="btn_client">Cadastrar</button>
                <div class="container-fluid">
                    <h4>Dados Gerais</h4>

                    <hr>

                    <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="nome" placeholder="Nome" id="nome" required>
                                </div>  
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="email" name="email" placeholder="E-mail" id="email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="date" name="data_nascimento" placeholder="Data de Nascimento" id="data_nascimento" required>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="cpf" placeholder="CPF" id="cpf" required>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <h4>Endereço</h4>
                        <hr>


                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="cep" placeholder="CEP" onchange="camposFormClient()" id="cep" minlength='8' required=true>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="logradouro" placeholder="Logradouro" onchange="camposFormClient()" id="logradouro">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" type="number" name="numero" placeholder="Número" onchange="camposFormClient()" id="numero" minlength=1 required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="complemento" placeholder="Complemento" onchange="camposFormClient()" id="complemento">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="bairro" placeholder="Bairro" onchange="camposFormClient()" id="bairro">
                                </div>  
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="cidade" placeholder="Cidade" onchange="camposFormClient()" id="cidade">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="estado" placeholder="Estado" onchange="camposFormClient()" id="estado">
                                </div>
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="longitude" placeholder="Longetude" id="lng" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" type="text" name="latitude" placeholder="Latitude" id="lat" disabled>
                                </div>
                            </div>

                            <hr>

                        </div>
                    </div>       
            </form>
            <h6 style="color:red;">* Todos os campos desse formulário são obrigatorios e devem serem preenchidos de forma sequencial.</h6>
        </div>
    </div>       
</div>
@endsection
