@extends('layouts.app')

@section('content')
<div class="container-fluid fadein loading">
    <div class="row">
        <div class="col-md-12">
            <div class="spin"></div>
        </div>
    </div>
</div>
<div class="container-fluid tabela">
    <div class="row justify-content-center">
            <a href="/extraircsv" class="btn btn-outline-danger btn-csv" id="btn_csv">Extrair CSV</a>
            {{--  <button type="button" class="btn btn-outline-danger btn-csv" data-toggle="modal" data-target="#uploadModal" id="uploadModal">Upload file</button>  --}}
            <button type="button" class="btn btn-outline-danger btn-csv" id="uploadModal">Upload file</button>
            <div class="col-md-12">
                    <table class="table table-bordered table-hover table-inverse table-responsive ">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>Cód. Cliente</th>
                                    <th>Nome</th>
                                    <th>E-mail</th>
                                    <th>Data de Nascimento</th>
                                    <th>CPF</th>
                                    <th>Endereço</th>
                                    <th>CEP</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clients as $client)
                                <tr>    
                                    <td>{{$client->id_client}}</td>
                                    <td>{{$client->nome}}</td>
                                    <td>{{$client->email}}</td>
                                    <td>{{$client->data_nascimento}}</td>
                                    <td class='cpf'>{{$client->cpf}}</td>
                                    <td>{{$client->logradouro}} - {{$client->cidade}} - {{$client->estado}}</td>
                                    <td class='cep'>{{$client->cep}}</td>
                                </tr>
                                @endforeach
                               
                            </tbody>
                        </table>
            </div>
        </div>
        
</div>
@endsection
