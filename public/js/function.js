
$(document).ready(function(){
    $(".cep").mask("99999-999");
    $(".cpf").mask("999.999.999-99");
    $("#cep").mask("99999-999");
    $("#cpf").mask("999.999.999-99");
    camposFormClient();
    spinOff();
    
    

    /**
     * Nesta etapa tratamos o serialize que foi formatado após os preenchimento dos campos 
     * passando os dados do formulario para a minha rota e de la fazendo insert
     */
    $('#frm_client').submit(function(e){
        e.preventDefault();
        var token = $("[name='_token']").val();
        var nome = $('#nome').val();
        var email = $('#email').val();
        var data_nascimento = $('#data_nascimento').val();
        var cpf = $('#cpf').val();
        var cep = $('#cep').val();
        var logradouro = $('#logradouro').val();
        var numero = $('#numero').val();
        var complemento = $('#complemento').val();
        var bairro = $('#bairro').val();
        var cidade = $('#cidade').val();
        var estado = $('#estado').val();
        var lng = $('#lng').val();
        var lat = $('#lat').val();
        $.ajaxSetup({
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
        });

        $.post('/registerclient',{
            _token: token,
            nome:nome,
            email:email,
            data_nascimento:data_nascimento,
            cpf:cpf,
            cep:cep,
            logradouro:logradouro,
            numero:numero,
            complemento:complemento,
            bairro:bairro,
            cidade:cidade,
            estado:estado,
            longitude:lng,
            latitude:lat},
            function(data){
                if(data == 1){
                    swal ("",  "Cliente cadastrado com sucesso!" ,  "success" );
                    window.setTimeout(function(){

                        window.location.href = "http://localhost:8000/";
                
                    }, 1000);
                }else{
                    swal ( "" ,  "O Cliente não foi cadastrado, entrar em contato com o administrador do sistema." ,  "error" )
                }
            }
        );
    });

    /**
     * Função principal na qual dispara o gatilho inicial de todas as funções que são precisas para que possa fazer o upload do csv.
     */
    $("#uploadModal").click(function() {
        $.FileDialog({

                accept: ".csv",
                cancel_button: "Fechar",
                drag_message: "Arraste e solte o arquivo aqui!",
                dropheight: 400,
                error_message: "Um erro ocorreu enquanto o arquivo estava sendo lido",
                multiple: false,
                ok_button: "Enviar!",
                readAs: "DataURL",
                remove_message: "Remove&nbsp;file",
                title: "Upload de Arquivos"

        }).on('files.bs.filedialog', function(ev) {
            let files = ev.files;

            if(window.FileReader){
                fileToRead = files[0];

                let reader = new FileReader()
                reader.readAsText(fileToRead);

                reader.onload= loadHendler;
                reader.onerror= errorHendler;
               
            }else{
                swal('','O Upload não pode ser efetuado, pois não foi suportado pelo Browser!','error');
                return false;
            }
            
            if(files.length == 0){
                swal('','O Upload não pode ser efetuado sem arquivos!','error');
                return false;
            }
           
            
        }).on('cancel.bs.filedialog', function(ev) {
            console.log('cancelado');
        });
    });
});
function loadHendler(event){
    const csv = event.target.result;
    spinOn();
    processData(csv);
}
function errorHendler(event){
    if(event.target.error.name == "NotReadableError"){
        console.log('Cannot read File');
    }
}
//FUNÇÃO PRINCIPAL DE TODO O ESCOPO NO QUAL VALIDA O CSV, PASSANDO O MESMO PARA JSON GARANTINDO MAIOR CONFIABILIDADE NOS DADOS!

function sendToMaps(line) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: 'http://maps.googleapis.com/maps/api/geocode/json?address=' + line.endereco + ' + ' + line.cep,
            dataType:'json',
            success: function(data) {  
                const user =  {
                    nome: line.nome,
                    email: line.email,
                    datanasc: line.datanasc,
                    cpf: line.cpf,
                };
                console.log(data.results[0].geometry.location);
                const { lat, lng } = data.results[0].geometry.location;
                user.lat = lat.toString();
                user.lng = lng.toString();
                
                for(const component of data.results[0].address_components) {
                    user[component.types[0]] = component.long_name;
                }
     
                
                $.get('/uploadcsv',{user},function(data){
                    resolve(true);
                });
               
            },
            error: function(data){
                reject(data);
            }
        });
    });
}

function camposFormClient(){

    const nome = $('#nome').val();
    const email = $('#email').val();
    const data_nascimento = $('#data_nascimento').val();
    const cpf = $('#cpf').val();
    const cep = $('#cep').val();
    const logradouro = $('#logradouro').val();
    const numero = $('#numero').val();
    const complemento = $('#complemento').val();
    const bairro = $('#bairro').val();
    const cidade = $('#cidade').val();
    const estado = $('#estado').val();
   
    $('#btn_client').prop("disabled", true);
    if( nome !== '' && 
        email !== '' && 
        data_nascimento !== '' && 
        cpf !== '' && 
        cep !== '' && 
        logradouro !== '' && 
        numero !== '' && 
        nome !== '' && 
        bairro !== '' && 
        cidade !== '' 
        && estado !== ''){
            $('#btn_client').prop("disabled", false);
              obj =   {
                endereco:logradouro+', '+numero+' '+complemento+'+'+bairro+'+'+cidade+'+'+estado+'+'+cep
            }
            $.ajax({
                type: "GET",
                url: 'http://maps.googleapis.com/maps/api/geocode/json?address='+obj.endereco,
                dataType:'json',
                    success: function(data)
                    {  
                        const request = {
                            nome:nome,
                            email:email,
                            data_nascimento:data_nascimento,
                            cpf:cpf,
                        };
                        for(const component of data.results[0].address_components) {
                            request[component.types[0]] = component.long_name;
                        }

                        var resultJson = data.results[0].geometry.location;
                        lat = resultJson.lat;
                        lng = resultJson.lng;
                        $('#lng').val(lng);
                        $('#lat').val(lat);                        
                        $('#logradouro').val(request.route.toUpperCase());
                        $('#numero').val(request.street_number.toUpperCase());
                        $('#bairro').val(request.political.toUpperCase());
                        $('#cidade').val(request.administrative_area_level_2.toUpperCase());
                        $('#estado').val(request.administrative_area_level_1.toUpperCase());
                        console.log("OK");
                        
                     },
                     error: function(data){
                        console.log(data)
                     }
            }); 
        
    }
}

function processData(csv) {    
    const allTextLines = csv.replace(/"/g, '').split(/\r\n|\n/);
    const lines = allTextLines.filter((element, index) => (element && index)).map(element => { 
        const splited = element.split(';');
        return {
            nome: splited[0],
            email: splited[1],
            datanasc: splited[2],
            cpf: splited[3],
            endereco: splited[4],
            cep: splited[5],
        };
    });
    
    const promises = [];
    for(const line of lines) {
        promises.push(sendToMaps(line));
    }

    Promise.all(promises).then((data) => {
        spinOff();
        swal("","Upload do arquivo efetuado com sucesso!","success");
        window.setTimeout(function(){

            window.location.href = "http://localhost:8000/";
    
        }, 1000);
    }).catch((err) => {
        swal("","Não foi possível efetuar o Upload do Arquivo, verifique a estrutura do arquivo ou contate um administrador do sistema","error");
    });
    
}

function spinOn(){
    $('.tabela').css("display","none");
    $('.loading').css("display","block");
}
function spinOff(){
    $('.tabela').css("display","block");
    $('.loading').css("display","none");
}
